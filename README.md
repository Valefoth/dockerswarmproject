# DockerSwarmProject

ANAS SOULHAT |
GABRIEL DUCHEMIN |
MAXIME VAUGOYEAU |
TITOUAN BONAMY

## Phase 1 et 3

Prérequis :
- Il faut au moins 3 VMs pour l'installation du cluster. (1 Manager et minimum 2 workers)
- Toutes les machines doivent avoir un accès à internet
- Les machines doivent accepter les connexions en ssh avec root avec un mot de passe pour le script d'installation.
- Tout l'environnement a été testé pour l'utilisateur `root`, toutes les manipulations suivantes sont à faire avec `root`

### Installation
1. Il faut remplir les variables d'environnements disponibles dans le fichier `stack/.env.example` et renommer le fichier en `stack/.env` :
    - __TRAEFIK_USERNAME__, utilisateur pour l'accès au dashboard traefik;
    - __TRAEFIK_PASSWORD__, le password pour l'accès au dashboard traefik;
    - __EXTERNAL_IP__, l'ip sur laquelle va être accessible le manager, le point d'entré du cluster;
    - __DOMAIN__, le domaine qui va être utiliser pour les résolutions par CoreDNS;
    - __HASHED_PASSWORD__, utilisé pour l'authentification de traefik sur le dashboard, a laisser tel quel;
    - __DB_PASSWORD__, le mot de passe pour la base de données wordpress;
    - __DB_USER__, l'utilisateur de la base de données wordpress;
    - __DB_NAME__, le nom de la base de données wordpress.

2. Il faut renseigner les IP des worker dans le fichier [`ansible/inventory`](./ansible/inventory), le script d'installation [`./scripts/install.sh`](./scripts/install.sh) se base dessus.
3. Il faut lancer le script d'installation sur le noeud qui sera manager pour l'environnement.
```sh
./scripts/install.sh
```
4. Dans le script, il faudra mettre le mot de passe des différents worker pour l'accès en ssh.

## Phase 2
- schéma infrastructure réseaux [`schema/NetworkDiagram.drawio.png`](./schema/NetworkDiagram.drawio.png)
- schéma infrastructure swarm [`schema/Swarm.drawio.png`](./schema/Swarm.drawio.png)

## Phase 3
### Configuration et accès à l'environnement
Pour accéder aux différents services, il faut utiliser ces urls :
- traefik.<domaine renseigné dans dans l'env>
- wordpress.<domaine renseigné dans dans l'env>
- grafana.<domaine renseigné dans dans l'env>

### Traefik
La configuration de traefik est faite dans la stack internal.

### Wordpress
Lors du premier accès, wordpress va demander une installation

### Grafana et Loki
- Il faut accéder a grafana par l'adresse grafana.<domaine renseigné dans dans l'env>
- L'utilisateur par défaut c'est admin avec le mot de passe admin
- Suivre ce tuto pour configurer grafana avec Loki : https://grafana.com/docs/loki/latest/getting-started/grafana/