ans-base: ans-role
	ansible-playbook -i ./ansible/inventory ./ansible/base.yaml
ans-role:
	ansible-playbook -i ./ansible/inventory ./ansible/roles.yaml
ans-docker:
	ansible-playbook -i ./ansible/inventory ./ansible/docker.yaml
update-internal-stack:
	docker stack rm internal
	. ./stack/.env && docker stack deploy -c stack/internal.yaml internal
update-wordpress-stack:
	. ./stack/.env && docker stack deploy -c stack/wordpress.yaml wordpress
update-monitoring-stack:
	. ./stack/.env && docker stack deploy -c stack/monitoring.yaml monitoring