#!/bin/sh

worker=$(cat ansible/inventory | grep -v "[worker]")

keyname="$HOME/.ssh/id_ed25519"
echo $keyname
if [ -e $keyname ]; then
    echo "SSH key is already present"
else
    ssh-keygen -q -f $keyname -t ed25519 -N ""
fi

for w in $worker; do
    ssh-copy-id -oStrictHostKeyChecking=no -i $keyname root@$w
done

DEBIAN_FRONTEND=noninteractive apt update ;apt install -y ansible make sudo;
for w in $worker
do
ssh -oStrictHostKeyChecking=no root@$w "DEBIAN_FRONTEND=noninteractive apt update ;apt install -y sudo python3;"
done
make ans-base; make ans-docker


echo "Enter the advertise address for the docker swarm manager"
read advaddr

docker swarm init --advertise-addr $advaddr
sleep 5
token=$(docker swarm join-token worker -q)

for w in $worker
do
ssh -oStrictHostKeyChecking=no root@$w "docker swarm join --token $token $advaddr:2377"
done

docker network create --driver=overlay --subnet=10.10.10.0/24 public

NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')

docker node update --label-add entrypoint=true $NODE_ID

make update-internal-stack
make update-wordpress-stack
make update-monitoring-stack

# Fin